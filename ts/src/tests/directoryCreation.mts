import { Directory } from "../index.mjs";


const projectRootDirectoryname = process.cwd();
// console.log('projectRootDirectoryname', projectRootDirectoryname)

const originalDirectory = new Directory(projectRootDirectoryname);

const compiledSourcePathname = 'es';

originalDirectory.attachNew(`${compiledSourcePathname}/src/tests/subdirectory-1/subdirectory-1.1`, true);
